# SPDX-FileCopyrightText: The maven-build-process Authors
# SPDX-License-Identifier: 0BSD

name: Perform Release
on:
  schedule:
    - cron:  15 1 * * FRI
  workflow_dispatch:
jobs:
  publish:
    runs-on: ubuntu-latest
    permissions:
      contents: write
    steps:
      - uses: actions/checkout@v3
      - uses: ./.github/actions/managed-pom
      - uses: actions/cache@v3
        with:
          path: ~/.m2/repository
          key: ${{ runner.os }}-maven-${{ hashFiles('**/pom.xml') }}
          restore-keys: |
            ${{ runner.os }}-maven-
      - name: Create release version
        id: release
        run: echo "version=$(date +'%Y.%-m.%-d')" >> $GITHUB_OUTPUT
      - name: Count commits in last week
        id: commits
        run: echo "count=$(git rev-list --count HEAD --since='last Friday')" >> $GITHUB_OUTPUT
      - name: Set release version
        if: steps.commits.outputs.count > 0
        run: mvn --batch-mode --define newVersion=${{ steps.release.outputs.version }} --define generateBackupPoms=false versions:set
      - id: gpg
        if: steps.commits.outputs.count > 0
        name: GPG Key
        run: echo "${{ secrets.GPG_SECRET_KEY_BASE64 }}" | base64 --decode > $GITHUB_WORKSPACE/signing.key.asc
      - name: Publish to Maven Central
        if: steps.commits.outputs.count > 0
        run: |
          mvn \
            --batch-mode \
            --activate-profiles release \
            --define pgp.secretkey=keyfile:$GITHUB_WORKSPACE/signing.key.asc \
            --define pgp.passphrase=literal:${{ secrets.GPG_SECRET_KEY_PASSWORD }} \
            deploy
        env:
          MAVEN_CENTRAL_USERNAME: ${{ secrets.MAVEN_CENTRAL_USERNAME }}
          MAVEN_CENTRAL_TOKEN: ${{ secrets.MAVEN_CENTRAL_TOKEN }}
      - id: create_release
        name: Create Release
        if: steps.commits.outputs.count > 0
        uses: softprops/action-gh-release@v1
        with:
          tag_name: ${{ steps.release.outputs.version }}
          name: ${{ steps.release.outputs.version }}
          draft: false
          prerelease: false
          generate_release_notes: true
      - id: mail
        name: Send Mail
        if: steps.commits.outputs.count > 0
        uses: dawidd6/action-send-mail@v3.7.1
        with:
          server_address: ${{ secrets.MAIL_SERVER }}
          server_port: ${{ secrets.MAIL_PORT }}
          username: ${{ secrets.MAIL_USERNAME }}
          password: ${{ secrets.MAIL_PASSWORD }}
          subject: ${{ github.event.repository.name }} version ${{ steps.release.outputs.version }} published
          body: See ${{ steps.create_release.outputs.url }} for details.
          to: announcements@metio.groups.io
          from: ${{ secrets.MAIL_SENDER }}
      - id: matrix
        name: Send Matrix Message
        if: steps.commits.outputs.count > 0
        uses: s3krit/matrix-message-action@v0.0.3
        with:
          room_id: ${{ secrets.MATRIX_ROOM_ID }}
          access_token: ${{ secrets.MATRIX_ACCESS_TOKEN }}
          message: ${{ github.event.repository.name }} version [${{ steps.release.outputs.version }}](${{ steps.create_release.outputs.url }}) published
          server: matrix.org
